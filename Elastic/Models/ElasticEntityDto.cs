﻿namespace Elastic.Models
{
    public class ElasticEntityDto
    {
        public Guid Id { get; set; }
        public virtual string SearchText { get; set; }
        public virtual double? Score { get; set; }
    }
}
