﻿using Elastic.Application;
using Elastic.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Elastic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ElasticController : ControllerBase
    {
        private readonly IElasticSearchManager _elasticSearchManager;

        public ElasticController(IElasticSearchManager elasticSearchManager)
        {
            _elasticSearchManager = elasticSearchManager;
        }

        // POST api/<ElasticController>
        [HttpPost("CreateIndex")]
        public async Task CreateIndex()
        {
            await _elasticSearchManager.CreateIndex();
        }

        // GET: api/<ElasticController>
        [HttpGet]
        public async Task<IEnumerable<ElasticEntityDto>> Get(string searchText)
        {
            var items = await _elasticSearchManager.SearchAsync(searchText);
            return items.Select(p =>
            new ElasticEntityDto
            {
                Id = p.Id,
                SearchText = p.SearchText,
                Score = p.Score
            });
        }

        // GET api/<ElasticController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<ElasticController>
        [HttpPost]
        public async Task Post([FromBody] ElasticEntityDto value)
        {
            await _elasticSearchManager.AddOrUpdateAsync(new ElasticEntity
            {
                Id = value.Id,
                SearchText = value.SearchText,
                Score = value.Score
            });
        }

        // PUT api/<ElasticController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<ElasticController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
