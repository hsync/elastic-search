﻿using Nest;

namespace Elastic.Application
{
    public class ElasticEntity
    {
        public Guid Id { get; set; }
        public virtual CompletionField Suggest { get; set; }
        public virtual string SearchText { get; set; }
        public virtual double? Score { get; set; }
    }
}
