﻿namespace Elastic.Application
{
    public class ElasticSearchOptions
    {
        public string HostUrls { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
