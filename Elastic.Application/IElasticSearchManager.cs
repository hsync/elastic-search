﻿using Nest;

namespace Elastic.Application
{
    public interface IElasticSearchManager
    {
        IElasticClient Client { get; set; }

        Task AddOrUpdateAsync(ElasticEntity esDocument);
        Task CreateIndex();
        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
        Task<List<ElasticEntity>> SearchAsync(string searchText, int? skipCount = null, int? maxResultCount = null);
    }
}