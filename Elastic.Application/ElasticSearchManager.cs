﻿using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Threading;

namespace Elastic.Application
{
    public class ElasticSearchManager : IElasticSearchManager
    {
        public const string IndexName = "hsync";
        private readonly ElasticSearchOptions _options;
        public IElasticClient Client { get; set; }
        public ElasticSearchManager(IOptions<ElasticSearchOptions> options)
        {
            _options = options.Value;
            Client = GetClient();
        }
        private ElasticClient GetClient()
        {
            var str = _options.HostUrls;
            var strs = str.Split('|');
            var nodes = strs.Select(s => new Uri(s)).ToList();

            var connectionString = new ConnectionSettings(new Uri(str))
                .CertificateFingerprint("B0:4D:DC:05:E6:A0:83:8A:A1:91:56:FD:67:97:2A:88:FE:12:28:F6:B2:91:0A:7C:4E:53:A4:FA:41:C2:59:0C")
                .DisablePing()
                .SniffOnStartup(false)
                .SniffOnConnectionFault(false);

            if (!string.IsNullOrEmpty(_options.UserName) && !string.IsNullOrEmpty(_options.Password))
                connectionString.BasicAuthentication(_options.UserName, _options.Password);

            connectionString.EnableApiVersioningHeader();
            return new ElasticClient(connectionString);
        }

        public async Task CreateIndex()
        {
            var exis = await Client.Indices.ExistsAsync(IndexName);

            if (exis.Exists)
                return;

            var result = await Client.Indices.CreateAsync(IndexName, c => c
                     .Map<ElasticEntity>(m =>
                     {
                         return m
                             .Properties(p => p
                                 .Keyword(x => x.Name(d => d.Id))
                                 .Keyword(x => x.Name(d => d.Suggest))
                                 .Keyword(x => x.Name(d => d.Score))
                                 .Text(x => x.Name(d => d.SearchText)));
                     }));

            if (result.Acknowledged)
            {
                return;
            }
            throw new Exception($"Create Index {IndexName} failed : :" + result.OriginalException);
        }

        public virtual async Task AddOrUpdateAsync(ElasticEntity esDocument)
        {
            var client = GetClient();
            await client.IndexAsync(esDocument, x => x.Index(IndexName));
        }

        public virtual async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            await GetClient()
                .DeleteAsync(DocumentPath<ElasticEntity>.Id(id.ToString("N")), x => x.Index(IndexName), cancellationToken);
        }

        public virtual async Task<List<ElasticEntity>> SearchAsync(string searchText, int? skipCount = null, int? maxResultCount = null)
        {
            var request = new SearchRequest
            {
                Size = maxResultCount ?? 10,
                From = skipCount ?? 0,
                Query = new BoolQuery
                {
                    Must = new QueryContainer[]
                   {
                        new MatchQuery
                        {
                            Field = "searchText",
                            Query = searchText,
                            
                        }
                   },
                   // Filter = new QueryContainer[]
                   //{
                   //     new BoolQuery
                   //     {
                   //         Must = new QueryContainer[]
                   //         {
                   //             new TermQuery
                   //             {
                   //                 Field = "projectId",
                   //                 Value = NormalizeField(projectId)
                   //             },
                   //             new TermQuery
                   //             {
                   //                 Field = "version",
                   //                 Value = NormalizeField(version)
                   //             },
                   //             new TermQuery
                   //             {
                   //                 Field = "languageCode",
                   //                 Value = NormalizeField(languageCode)
                   //             }
                   //         }
                   //     }
                   //}
                },
                Highlight = new Highlight
                {
                    PreTags = new[] { "<highlight>" },
                    PostTags = new[] { "</highlight>" },
                    Fields = new Dictionary<Field, IHighlightField>
                    {
                        {
                            "searchText", new HighlightField()
                        }
                    }
                }
            };

            var response = await Client.SearchAsync<ElasticEntity>(request);

            var docs = new List<ElasticEntity>();
            foreach (var hit in response.Hits)
            {
                var doc = hit.Source;
                docs.Add(doc);
            }

            return docs;

        }

        private object NormalizeField(Guid projectId)
        {
            throw new NotImplementedException();
        }
    }
}
